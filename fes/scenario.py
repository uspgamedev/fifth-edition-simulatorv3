
from random import choice
from collections import defaultdict

from fes.dice import D6
from fes.database import DB
from fes.character import Character
from fes.combat import Combat
from fes.combat_statistics import CombatStatistics


class Scenario:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.combat_spec = kwargs.get('combatants')
        self.times = kwargs.get('times')
        self.stats = {}
        for combatant in self.combat_spec:
            self.stats[combatant['name']] = CombatStatistics()

    @staticmethod
    def db_name():
        return "scenarios"

    @staticmethod
    def create(**kwargs):
        return Scenario(**kwargs)

    def run(self):
        for _ in range(self.times):
            combat = Combat()
            for combatant in self.combat_spec:
                char = DB.get(Character, combatant['name'])
                for _ in range(combatant['level']):
                    char.increase_level()
                combat.add_combatant(char)
            combat.run()

            for combatant in combat.combatants:
                self.stats[combatant.name] += combatant.statistics

    def report(self):
        print(self.name)
        for name, stats in self.stats.items():
            print("{}:".format(name))
            print("  Win rate: {}%".format(100 * stats.wins/self.times))
            print("  Hit rate: {:.2f}%"
                  .format(100*stats.hits/stats.attacks))
            print("  Average damage: {:.2f}"
                  .format(stats.damage/stats.hits))
