
from random import randrange
from collections import namedtuple


Result = namedtuple('Result', ['value', 'crit', 'miss'])


class Formulable:
    def __add__(self, other):
        return Formula(self, other)

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        return self + Formula(-other)

    def __mul__(self, other):
        return Formula(*[Formula(self) for i in range(other)])

    def __rmul__(self, other):
        return self * other


class Dice(Formulable):
    def __init__(self, sides):
        self.sides = sides

    def roll(self, advantage=False, disadvantage=False):
        if advantage and not disadvantage:
            return 1 + max([randrange(self.sides), randrange(self.sides)])
        elif disadvantage and not advantage:
            return 1 + min([randrange(self.sides), randrange(self.sides)])
        return 1 + randrange(self.sides)

D4 = Dice(4)
D6 = Dice(6)
D8 = Dice(8)
D10 = Dice(10)
D12 = Dice(12)
D20 = Dice(20)
D100 = Dice(100)

DICES = {
    "d4": D4,
    "d6": D6,
    "d8": D8,
    "d10": D10,
    "d12": D12,
    "d20": D20,
    "d100": D100
}


class Formula(Formulable):
    def __init__(self, *operands):
        self.dices = []
        self.numbers = []
        self.rolls = []
        for operand in operands:
            if isinstance(operand, Dice):
                self.dices.append(operand)
            elif isinstance(operand, Formula):
                self.dices.extend(operand.dices)
                self.numbers.extend(operand.numbers)
            else:
                self.numbers.append(operand)

    @staticmethod
    def from_dict(**kwargs):
        operands = []
        for key, amount in kwargs.items():
            if key == "extra":
                operands.append(amount)
            else:
                operands.append(DICES[key])
        return Formula(*operands)

    def roll_dice(self, advantage=False, disadvantage=False):
        self.rolls = [dice.roll(advantage, disadvantage)
                      for dice in self.dices]

    def eval(self, advantage=False, disadvantage=False):
        self.roll_dice(advantage, disadvantage)
        return self.make_result()

    def eval_crit(self):
        self.roll_dice()
        self.rolls.extend([dice.roll() for dice in self.dices])
        return self.make_result()

    def make_result(self):
        value = sum(self.rolls) + sum(self.numbers)
        crit = None
        miss = None
        if len(self.dices) == 1 and self.dices[0].sides == 20:
            crit = self.rolls[0] == 20
            miss = self.rolls[0] == 1
        return Result(value, crit, miss)


