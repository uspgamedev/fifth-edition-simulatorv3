
from math import ceil
from collections import defaultdict

from fes.dice import D20
from fes.equipment import Equipment, Weapon, Armor, UNARMED_STRIKE
from fes.race import Race
from fes.character_class import Class
from fes.database import DB
from fes.combat_statistics import CombatStatistics


def proficiency(lv):
    return ceil(lv/4) + 1


def modifier(score):
    return (score - 10) // 2


class Character:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.damage = 0
        self.ability_scores = kwargs.get('ability-scores')
        self.inventory = []
        self.equipped = {'weapon': UNARMED_STRIKE, 'armor': None}
        self.race = DB.get(Race, kwargs.get('race'))
        self.class_levels = []
        for class_name in kwargs.get('class-levels'):
            self.class_levels.append(DB.get(Class, class_name))
        self.level = 0
        self.effects = {}
        self.race.apply_effects(self)
        self.statistics = CombatStatistics()
        self.next_action = None

    @staticmethod
    def db_name():
        return 'characters'

    def add_effect(self, effect):
        if effect['type'] not in self.effects.keys():
            self.effects[effect['type']] = []
        self.effects[effect['type']].append(effect)

    def effects_with_type(self, type):
        return self.effects[type]

    def effects_with_tag(self, tag):
        result = []
        for effect_type in self.effects.values():
            for effect in effect_type:
                if tag in effect.get('tags', []):
                    result.append(effect)
        return result

    def has_effect_tag(self, tag):
        return len(self.effects_with_tag(tag)) > 0

    def increase_level(self):
        newclass = self.class_levels[self.level]
        self.level += 1
        count = defaultdict(int)
        for lv in range(self.level):
            count[self.class_levels[lv-1].name] += 1
        newclass.apply_effects(self, count[newclass.name], self.level == 1)

    def total_hit_points(self):
        total = 0
        for hitpoints in self.effects_with_type('hit-points'):
            total += hitpoints['amount']
        return total + self.level * self.get_ability_modifier("CON")

    def get_proficiencies(self):
        proficiencies = {}
        for prof in self.effects_with_type('proficiency'):
            proficiencies[prof['which']] = True
        return proficiencies

    def proficiency_bonus(self, which=None):
        profs = self.get_proficiencies()
        if which is None or which in profs:
            return proficiency(self.level)
        else:
            return 0

    @staticmethod
    def create(**kwargs):
        char = Character(**kwargs)
        for itemname in kwargs.get('inventory'):
            char.receive_equipment(DB.get(Equipment, itemname))
        for index in kwargs.get('equipped'):
            char.equip(char.inventory[index])
        return char

    def get_ability_score(self, which):
        change = 0
        for abscore_change in self.effects_with_type('ability-score-change'):
            if abscore_change['which'] == which:
                change += abscore_change['value']
        return self.ability_scores[which] + change

    def get_ability_modifier(self, which):
        return modifier(self.get_ability_score(which))

    def current_hit_points(self):
        return self.total_hit_points() - self.damage

    def alive(self):
        return self.current_hit_points() > 0

    def dead(self):
        return not self.alive()

    def armor_class(self):
        armor = self.equipped['armor']
        if armor is None:
            return 10 + self.get_ability_modifier('DEX')
        if armor.max_dex is not None:
            return armor.ac_base + min(armor.max_dex,
                                       self.get_ability_modifier('DEX'))
        return armor.ac_base + self.get_ability_modifier('DEX')

    def receive_damage(self, damage):
        self.damage = min(self.damage + damage, self.total_hit_points())

    def receive_attack(self, to_hit, damage):
        if to_hit.crit or to_hit.value >= self.armor_class():
            self.receive_damage(damage)
            return True
        return False

    def weapon_damage_formula(self):
        return self.equipped['weapon'].damage_formula

    def equip(self, equipment):
        if equipment in self.inventory:
            if isinstance(equipment, Weapon):
                self.equipped['weapon'] = equipment
            if isinstance(equipment, Armor):
                self.equipped['armor'] = equipment

    def unequip(self, equipment_part):
        if equipment_part == 'weapon':
            self.equipped['weapon'] = UNARMED_STRIKE
        else:
            self.equipped[equipment_part] = None

    def roll_initiative(self):
        return (D20 + self.get_ability_modifier('DEX')).eval().value

    def melee_attack(self, target):
        attack_roll_formula = D20 + self.get_ability_modifier('STR') + \
            max(self.proficiency_bonus(self.equipped['weapon'].name),
                self.proficiency_bonus(self.equipped['weapon'].group))
        damage_roll_formula = self.weapon_damage_formula() + \
            self.get_ability_modifier('STR')
        attack_roll_result = attack_roll_formula.eval(
            False,
            target.has_effect_tag('attack-roll-disadvantage-against-self')
        )
        if attack_roll_result.miss:
            # print('{} attacked {} with {} and missed! (rolled 1)'.format(
            #    self.name, target.name, self.equipped['weapon']))
            return (attack_roll_result.value, False, 0)
        damage_result = damage_roll_formula.eval_crit(
        ) if attack_roll_result.crit else damage_roll_formula.eval()
        hit = target.receive_attack(attack_roll_result, damage_result.value)
        # print('{} attacked {} with {} and rolled {} ({}) to cause {} damage'
        #       .format(self.name, target.name, self.equipped['weapon'],
        #               attack_roll_formula.rolls[0], attack_roll_result.value,
        #               damage_result.value))
        return (attack_roll_result.value, hit, damage_result.value)

    def receive_equipment(self, equipment):
        self.inventory.append(equipment)

    def choose_target(self, options):
        for option in options:
            if option != self:
                return option

    def choose_action(self):
        if D20.roll() > 10:
            self.next_action = self.attack
        else:
            self.next_action = self.dodge

    def execute_action(self, combatants):
        self.next_action(combatants)

    def attack(self, combatants):
        # print("{} chose attack".format(self.name))
        target = self.choose_target(combatants)
        hit, check, dmg = self.melee_attack(target)
        self.statistics.add_data(
        {
            'name': 'attack',
            'target': target,
            'hit-success': check,
            'damage-dealt': dmg
        })

    def dodge(self, combatants):
        # print("{} chose dodge".format(self.name))
        self.add_effect({
            'type': 'dodge',
            'tags': ['attack-roll-disadvantage-against-self'],
            'triggers': {
                'begin-turn': 'end'
            }
        })
        self.statistics.add_data(
        {
            'name': 'dodge',
            'target': self
        })

    def won(self):
        self.statistics.add_data({'name': 'win'})

    def trigger(self, name):
        removed = []
        for effects in self.effects.values():
            for effect in effects:
                trigger = effect.get('triggers', {}).get(name)
                if trigger == 'end':
                    removed.append(effect)
        for effect in removed:
            self.effects[effect['type']].remove(effect)

    def __str__(self):
        string = self.name
        string += "\nLevel {}".format(self.level)
        string += "\nProficiency Bonus: {}".format(self.proficiency_bonus())
        for ability in self.ability_scores.keys():
            score = self.get_ability_score(ability)
            mod = self.get_ability_modifier(ability)
            mod = '+{}'.format(mod) if mod > 0 else mod
            string += '\n{} : {} ({})'.format(ability, score, mod)
        string += '\nHit points {}/{}'.format(self.current_hit_points(),
                                              self.total_hit_points())
        string += '\nArmor Class {}'.format(self.armor_class())
        return string

