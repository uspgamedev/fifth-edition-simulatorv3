from pathlib import Path
import json


class Database:
    def __init__(self):
        self.data = {}
        db_dir = Path('./fes/database')
        for dir in db_dir.iterdir():
            key = str(dir).split('/')[-1]
            self.data[key] = {}
            for datafile in dir.iterdir():
                data = json.loads(open(datafile).read())
                self.data[key][data["name"]] = data

    def get(self, clazz, name):
        return clazz.create(**self.data[clazz.db_name()][name])

    def set(self, class_name, name, data):
        if name not in self.data[class_name].keys():
            self.data[class_name][name] = data

    def remove(self, class_name, name):
        if name not in self.data[class_name].keys():
            del self.data[class_name][name]

    def all(self, class_name):
        return self.data[class_name]

DB = Database()
