
from fes.database import DB
from fes.dice import DICES


class Class:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.hit_die = DICES[kwargs.get('hit-die')]
        self.features_per_level = kwargs.get('features')

    @staticmethod
    def db_name():
        return "classes"

    @staticmethod
    def create(**kwargs):
        return Class(**kwargs)

    def apply_effects(self, char, level, first_level=False):
        for effect in self.features_per_level[level-1]:
            char.add_effect(effect)
        hitpoint_fx = {"type": "hit-points"}
        if first_level:
            hitpoint_fx['amount'] = self.hit_die.sides
        else:
            hitpoint_fx['amount'] = self.hit_die.roll()
        char.add_effect(hitpoint_fx)
