from fes.dice import Formula


class Equipment:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', "UNKNOWN")
        self.weight = kwargs.get('weight', 0)
        self.cost = kwargs.get('cost', 0)

    def __str__(self):
        return self.name

    @staticmethod
    def db_name():
        return 'equipment'

    @staticmethod
    def create(**kwargs):
        category = kwargs.get('category')
        if category == 'Weapon':
            return Weapon(**kwargs)
        elif category == 'Armor':
            return Armor(**kwargs)


class Weapon(Equipment):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        damage = kwargs.get('damage')
        self.damage_formula = Formula.from_dict(**damage['formula'])
        self.type = damage['type']
        typeparts = kwargs.get('type').split(' ')
        self.group = typeparts[0]
        self.clazz = typeparts[1]


UNARMED_STRIKE = Weapon(name='Unarmed Strike',
                        damage={'formula': {'extra': 1},
                                'type': 'bludgeoning'},
                        category='Weapon',
                        type='Simple Melee',
                        cost=0,
                        weight=0)


class Armor(Equipment):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ac_base = kwargs.get('armor-class')
        self.type = kwargs.get('type')
        self.max_dex = kwargs.get('max-dex')
        self.min_strength = kwargs.get('min-str')
        self.stealth_disadvantage = kwargs.get('stealth')

