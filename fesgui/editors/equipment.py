
import uuid
import imgui
import json
from fes.database import DB


class EquipmentEditor:
    def __init__(self, name=None):
        if name is None:
            self.old_name = ''
            self.equipment_id = uuid.uuid4()
            self.equipment = {
                'name': ''
            }
        else:
            self.equipment = DB.get('equipment', name)

    def save(self):
        f = open("fes/database/equipment/{}.json".format(self.equipment_id),
                 'w')
        f.write(json.dumps(self.equipment))
        f.close()
        if self.equipment['name'] != self.old_name:
            DB.remove('equipment', self.old_name)
        DB.set('equipment', self.equipment['name'], self.equipment)

    def process(self):
        expanded, opened = imgui.begin("Equipment Editor", True)
        update = False
        if expanded and opened:
            changed, new_name = imgui.input_text("Name",
                                                 self.equipment['name'], 256)
            update |= changed
            if update:
                self.old_name = self.equipment['name']
                self.equipment['name'] = new_name
        # close current window context
        imgui.end()
        self.save()
        return opened, None

